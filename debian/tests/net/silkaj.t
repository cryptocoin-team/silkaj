use strict;
use warnings;

use Test::More;
use Test::Command::Simple;

# network access may be absent or unreliable
plan skip_all => 'network test skipped when EXTENDED_TESTING is unset'
	unless ($ENV{EXTENDED_TESTING});

my $CMD = 'silkaj';

run_ok $CMD, qw(blockchain info);
like stdout, qr/Current block number: \d/, 'info, stdout';
cmp_ok stderr, 'eq', '', 'info, stderr';

done_testing;
