use strict;
use warnings;

use Test::More;
use Test::Command::Simple;

my $CMD = 'silkaj';

run_ok $CMD;
like stdout, qr/^Usage: $CMD/, 'bare command, stdout';
cmp_ok stderr, 'eq', '', 'bare command, stderr';

run_ok $CMD, qw(--version);
like stdout, qr/^$CMD, version/, 'version, stdout';
cmp_ok stderr, 'eq', '', 'version, stderr';

run_ok $CMD, qw(--help);
like stdout, qr/^Usage: $CMD/, 'help, stdout';
cmp_ok stderr, 'eq', '', 'help, stderr';

run_ok $CMD, qw(about);
like stdout, qr/\bRepository:/, 'about, stdout';
cmp_ok stderr, 'eq', '', 'about, stderr';

done_testing;
