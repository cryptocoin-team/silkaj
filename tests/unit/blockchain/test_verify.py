# Copyright  2016-2022 Maël Azimi <m.a@moul.re>
#
# Silkaj is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Silkaj is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Silkaj. If not, see <https://www.gnu.org/licenses/>.

import pytest
from duniterpy.documents import Block

from silkaj.blockchain import verify

G1_INVALID_BLOCK_SIG = 15144

invalid_signature = "fJusVDRJA8akPse/sv4uK8ekUuvTGj1OoKYVdMQQAACs7Oaw\
DfpsV6cEMPcXxrQTCTRMrTN/rRrl20hN5zC9DQ=="
invalid_block_raw = "Version: 10\nType: Block\nCurrency: g1\nNumber: 15144\nPoWMin: 80\n\
Time: 1493683741\nMedianTime: 1493681008\nUnitBase: 0\n\
Issuer: D9D2zaJoWYWveii1JRYLVK3J4Z7ZH3QczoKrnQeiM6mx\nIssuersFrame: 106\n\
IssuersFrameVar: 0\nDifferentIssuersCount: 21\n\
PreviousHash: 0000033D8562368F1B099E924A4A83119BDA0452FAB2A8A4F1B1BA11F5450597\n\
PreviousIssuer: 5WD4WSHE96ySreSwQFXPqaKaKcwboRNApiPHjPWB6V9C\nMembersCount: 98\n\
Identities:\nJoiners:\nActives:\nLeavers:\nRevoked:\nExcluded:\nCertifications:\n\
Transactions:\nInnerHash: 8B194B5C38CF0A38D16256405AC3E5FA5C2ABD26BE4DCC0C7ED5CC9824E6155B\n\
Nonce: 30400000119992\n"

valid_signature = "qhXtFtl6A/ZL7JMb7guSDlxiISGsHkQ4hTz5mhhdZO0KCLqD2T\
mvjcGpUFETBSdRYVacvFYOvUANyevlcfx6Ag=="
valid_block_raw = "Version: 11\nType: Block\nCurrency: g1-test\nNumber: 509002\n\
PoWMin: 60\nTime: 1580293955\nMedianTime: 1580292050\nUnitBase: 2\n\
Issuer: 5B8iMAzq1dNmFe3ZxFTBQkqhq4fsztg1gZvxHXCk1XYH\nIssuersFrame: 26\n\
IssuersFrameVar: 0\nDifferentIssuersCount: 5\n\
PreviousHash: 0000EC4030E92E85F22F32663F5ABE137BA01CE59AF2A96050877320174C4A90\n\
PreviousIssuer: Dz37iRAXeg4nUsfVH82m61b39HK5fqm6Bu7mM2ujLYz1\nMembersCount: 11\n\
Identities:\nJoiners:\nActives:\nLeavers:\nRevoked:\nExcluded:\nCertifications:\n\
Transactions:\nInnerHash: 19A53ABFA19EC77B6360E38EA98BE10154CB92307F4909AE49E786CA7149F8C6\n\
Nonce: 10099900003511\n"


@pytest.mark.parametrize(
    "signature, block_raw",
    [(invalid_signature, invalid_block_raw), (valid_signature, valid_block_raw)],
)
def test_verify_block_signature(signature, block_raw):
    # Check with valid and non-valid signatures block
    invalid_signatures_blocks = []
    block = Block.from_signed_raw(block_raw + signature + "\n")
    verify.verify_block_signature(invalid_signatures_blocks, block)
    if block.number == G1_INVALID_BLOCK_SIG:
        assert invalid_signatures_blocks == [block.number]
    else:
        assert invalid_signatures_blocks == []


@pytest.mark.parametrize(
    "from_block, to_block, invalid_blocks_signatures",
    [(0, 5, []), (100, 500, [53]), (470, 2341, [243, 453])],
)
def test_display_result(from_block, to_block, invalid_blocks_signatures, capsys):
    expected = f"Within {from_block}-{to_block} range, "
    if invalid_blocks_signatures:
        expected += "blocks with a wrong signature: "
        expected += " ".join(str(n) for n in invalid_blocks_signatures)
    else:
        expected += "no blocks with a wrong signature."
    verify.display_result(from_block, to_block, invalid_blocks_signatures)
    captured = capsys.readouterr()
    assert expected + "\n" == captured.out
