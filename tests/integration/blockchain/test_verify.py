# Copyright  2016-2022 Maël Azimi <m.a@moul.re>
#
# Silkaj is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Silkaj is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Silkaj. If not, see <https://www.gnu.org/licenses/>.

import pytest
from click.testing import CliRunner
from duniterpy.api import bma

from silkaj import cli
from silkaj.blockchain import verify
from silkaj.constants import (
    BMA_MAX_BLOCKS_CHUNK_SIZE,
    FAILURE_EXIT_STATUS,
    SUCCESS_EXIT_STATUS,
)
from silkaj.network import client_instance

G1_INVALID_BLOCK_SIG = 15144
HEAD_BLOCK = 48000


def current(self):
    return {"number": HEAD_BLOCK}


@pytest.mark.parametrize(
    "from_block, to_block", [(2, 1), (20000, 15000), (0, HEAD_BLOCK + 1), (300000, 0)]
)
def test_check_passed_blocks_range(from_block, to_block, capsys, monkeypatch):
    monkeypatch.setattr(bma.blockchain, "current", current)
    client = client_instance()
    # https://medium.com/python-pandemonium/testing-sys-exit-with-pytest-10c6e5f7726f
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        verify.check_passed_blocks_range(client, from_block, to_block)
    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == FAILURE_EXIT_STATUS
    captured = capsys.readouterr()
    if to_block == HEAD_BLOCK + 1:
        expected = (
            f"Passed TO_BLOCK argument is bigger than the head block: {str(HEAD_BLOCK)}"
        )
    else:
        expected = "TO_BLOCK should be bigger or equal to FROM_BLOCK\n"
    assert expected in captured.out


@pytest.mark.parametrize(
    "from_block, to_block",
    [
        (G1_INVALID_BLOCK_SIG, G1_INVALID_BLOCK_SIG),
        (G1_INVALID_BLOCK_SIG - 5, G1_INVALID_BLOCK_SIG + 5),
        (1, 10),
        (HEAD_BLOCK - 1, 0),
    ],
)
def test_verify_blocks_signatures(from_block, to_block, monkeypatch):
    monkeypatch.setattr(bma.blockchain, "current", current)
    result = CliRunner().invoke(
        cli.cli, ["blockchain", "verify", str(from_block), str(to_block)]
    )
    assert result.exit_code == SUCCESS_EXIT_STATUS
    if to_block == 0:
        to_block = HEAD_BLOCK
    expected = f"Within {from_block}-{to_block} range, "
    if from_block in (1, HEAD_BLOCK - 1):
        expected += "no blocks with a wrong signature."
    else:
        expected += "blocks with a wrong signature: " + str(G1_INVALID_BLOCK_SIG)
    assert expected + "\n" in result.output


@pytest.mark.parametrize(
    "from_block, to_block, chunks_from, chunk_from",
    [
        (140, 15150, [140, 5140, 10140, 15140], 140),
        (140, 15150, [140, 5140, 10140, 15140], 15140),
        (0, 2, [0], 0),
    ],
)
def test_get_chunk_size(from_block, to_block, chunks_from, chunk_from):
    chunk_size = verify.get_chunk_size(from_block, to_block, chunks_from, chunk_from)
    if chunk_from != chunks_from[-1]:
        assert chunk_size == BMA_MAX_BLOCKS_CHUNK_SIZE
    else:
        assert chunk_size == to_block + 1 - chunk_from


@pytest.mark.parametrize("chunk_size, chunk_from", [(2, 1), (5, 10)])
def test_get_chunks(chunk_size, chunk_from):
    client = client_instance()
    chunk = verify.get_chunk(client, chunk_size, chunk_from)
    assert chunk[0]["number"] + chunk_size - 1 == chunk[-1]["number"]
