# Install Silkaj with pipx

By installing Silkaj with `pipx` or `pip` from PyPI, you get latest Silkaj version
which might not already be available into your favorite OS.

You have to use a shell on Unix system.

## Unix

### Install libsodium

```bash
# Debian
sudo apt install libsodium23

# Fedora
sudo dnf install libsodium

# macOS
brew install libsodium
```

### Install `pipx`

Check [`pipx` documentation](https://pypa.github.io/pipx/) for an extended installation tutorial.

```bash
# Debian
sudo apt install pipx python3-dev

# Fedora
sudo dnf install pipx python3-devel

# macOS
brew install python3 pipx
```

Following packages might be necessary:

```bash
sudo apt install libffi-dev python3-wheel libssl-dev
```

## Install with pipx

[pipx](https://pypa.github.io/pipx/) tool is recommended for Python executables.
It also seperate libraries into virtual environments.

### Ensure path

```bash
pipx ensurepath
```

### Install

```bash
pipx install silkaj
```

### Upgrade

```bash
pipx upgrade silkaj
```

### Uninstall

```bash
pipx uninstall silkaj
```

### Check Silkaj is working

```bash
silkaj
```

______________________________________________________________________

## Install with pip

### Install `pip`

```bash
# Debian
sudo apt install python3-pip

# Fedora
sudo dnf install python3-pip

# macOS: already installed with python3
```

### Completing `PATH`

After intallation, if you get a `bash: silkaj: command not found` error, you should add `~/.local/bin` to your `PATH`:

```bash
echo "export PATH=$PATH:$HOME/.local/bin" >> $HOME/.bashrc
source $HOME/.bashrc
```

### Install for current user only

```bash
pip3 install silkaj --user
```

### Upgrade

```bash
pip3 install silkaj --user --upgrade
```

### Uninstall (useful to see the real paths)

```bash
pip3 uninstall silkaj --user
```

### Check Silkaj is working

```bash
silkaj
```
